var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
	name : String,
	provider : String,
	provider_id : {type: String, unique: true},
	photo: String,
	createdAt : {type: Date, default: Date.now} 
});

// Exportamos el modelo 'User' para probarlo en otras
// partes de la aplicación.
var User = mongoose.model('User', UserSchema);