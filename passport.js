var mongoose = require ('mongoose');
var User = mongoose.model('User');
// Estrategia de autenticación con Twitter
var TwitterStrategy = require('passport-twitter').Strategy;
// Estrategia de autenticación con Facebook
var FacebookStrategy = require('passport-facebook').Strategy;
// Fichero de configuración donde se encuentran las API keys
// Este fichero no debe subirse a GitHub ya que contiene datos
// que puedan comporometer la seguridad de la aplicación. 
var config = require('./config');

// Exportamos como módulo las funciones de passport, de manera que
// podamos utlilizarlos en otras partes de la aplicación.
// De esta manera, mantenemos el código separado en varios archivos
// logrando que sea más manejable.
module.exports = function(passport) {

	// Serializa al usuario para almacenarlo en la sesion
	passport.serializeUser(function(user, done){
		done(null, user);
	});

	// Deserializa el objeto almacenado en la sesión para
	// poder utilizarlo
	passport.deserializeUser(function(obj, done){
		done(null, obj);
	});

	// Configuración del autenticado con Twitter
	/* passport.use(new TwitterStrategy({
		consumerKey : config.twitter.key,
		consumerSecret : config.twitter.secret,
		callbackURL : '/auth/twitter/callback'
	}, function(accessToken, refreshToken, profile, done) {
		// Busca en la base de datos si el usuario ya se autentico xxxx
		// momento y ya esta almacenado en ella.
		User.findOne({provider_id: profile.id}, function (err, user) {
			if(err) throw(err);

			// Si existe en la base de datos lo devuelve
			if(!err && user!=null) return done(null, user);

			// Si no existe, crea un nuevo objeto usuario
			var user = new User({
				provider_id : profile.id,
				provider : profile.provider,
				name : profile.displayName,
				photo : profile.photos[0].value
			});

			user.save(function(err) {
				if(err) throw err;
				done(null, user);
			});
		});
	})); */

var FACEBOOK_APP_ID = '168747743484669',
    FACEBOOK_APP_SECRET = '80a7fda2af63e70a6d80c0d9bdf3fcfe';

	// Configuración del autenticado con Facebook
	passport.use(new FacebookStrategy({
		clientID : FACEBOOK_APP_ID,
		clientSecret : FACEBOOK_APP_SECRET,
		callbackURL : '/auth/facebook/callback',
		profileFields : ['id', 'displayName', 'provider', 'photos']
	}, function(accessToken, refreshToken, profile, done) {
		// Busca en la base de datos si el usuario ya se autentico xxxx
		// momento y ya esta almacenado en ella.
		User.findOne({provider_id: profile.id}, function (err, user) {
			if(err) throw(err);

			// Si existe en la base de datos lo devuelve
			if(!err && user!=null) return done(null, user);

			// Si no existe, crea un nuevo objeto usuario
			var user = new User({
				provider_id : profile.id,
				provider : profile.provider,
				name : profile.displayName,
				photo : profile.photos[0].value
			});

			user.save(function(err) {
				if(err) throw err;
				done(null, user);
			});
		});
	}));
}